from sys import argv
import sqlite3

# for this sqlite3 demo we'll use some data from the SQL Island game
# check it out (it is in german though): https://sql-island.informatik.uni-kl.de

# ------- Config section -------

# in which file is the database stored (will be created if it does not exist)
DATABASE_FILE = 'sqlite_demo.db'
# whether some initial data should be written into the tables (set this to False after first use)
POPULATE_INITIAL_DATA = True
# a dictionary containing some initial data for the database
INITIAL_DATA = {
    'dorf': [
        (1, 'Affenstadt', 1),
        (2, 'Gurkendorf', 6),
        (3, 'Zwiebelhausen', 13),
    ]
}

# ------- the main script -------

# create a connection to the database (file)
db = sqlite3.connect(DATABASE_FILE)

# a cursor is common database concept to execute queries and retrieve results
cursor = db.cursor()

# let's create a first table (only if it does not already exist)
# but let's prepare our SQL query first
query = '''
CREATE TABLE IF NOT EXISTS dorf (
    dorfnr INTEGER,
    name TEXT,
    haeuptling INTEGER
)
'''
# now execute this query
cursor.execute(query)

# and commit all our changes to the databse, before we continue
db.commit()

# should we populate the database tables with some initial data?
if POPULATE_INITIAL_DATA:
    for dorfnr, name, haeuptling in INITIAL_DATA['dorf']:
        query = 'INSERT INTO dorf (dorfnr, name, haeuptling) VALUES (?, ?, ?)'
        params = (dorfnr, name, haeuptling)
        cursor.execute(query, params)
    # is there more to initialise?
    # after all inserts, we should commit the changes to the db again
    db.commit()

# if a dorf number was provided on the command line, search for the dorf
if len(argv) > 1:
    dorfnr = argv[1]
    # while we could do something like this:
    # cursor.execute('SELECT * FROM dorf WHERE dorfnr = ' + dorfnr)
    # we MUST ALWAYS use parameterized queries when using user input, to prevent SQL injections
    cursor.execute('SELECT * FROM dorf WHERE dorfnr = ?', (dorfnr,))
    # let's fetch all results (can there be more than one?) and display them
    results = cursor.fetchall()
    print('Found Dörfer:')
    for row in results:
        print(f'ID: {row[0]} Name: {row[1]} (Häuptling ID: {row[2]})')

# we should always close the database connection (in this case the file)
# as soon as we don't need it anymore
db.close()
