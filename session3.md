# Session 3 - Dictionaries, Packages, Exceptions, Regex and Classes

## Materialien zur Vorbereitung

Bitte diese Materialien vor der Session 3 anschauen:

* freecodecamp: [Dictionaries](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjHy96cwPf8AhWbRvEDHYO3DUsQn_QFKA56BAgMEDk&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DeWRfhZUzrAc%26t%3D6829&usg=AOvVaw2uObbvpHsuEoYYgTuW4k6W) (Video, ~20 min)
* freecodecamp: [Exceptions](https://www.google.com/url?sa=t&rct=j&q=&esrc=s&source=web&cd=&ved=2ahUKEwjHy96cwPf8AhWbRvEDHYO3DUsQn_QFKBR6BAgMED8&url=https%3A%2F%2Fwww.youtube.com%2Fwatch%3Fv%3DeWRfhZUzrAc%26t%3D11490&usg=AOvVaw1Klqf0oL_kmdoB5kJlpky9) (Video, ~8 min)
* PyLadies Vienna: [Classes](https://pyladies.at/2022/pyladies-en-vienna-2022-autumn/beginners-en/class/) (Text)


## Session Task

### Challenge 1: Bau ein Wörterbuch (Deutsch/Englisch oder Deutsch/Wienerisch oder Deutsch/anything)

Schreibe dein eigenes Wörterbuch. Baue auch eine zweite Ebene ein.
 
```python
thisdict = {
  "Blume": "Flor",
  "Haus": "Casa",
  "Auto": {
    "Auto_spanien": "Coche",
    "Auto_latino": "Carro"
  }
}
```
Gib dann mit einer `for` - Schleife dein gesamtes Wörterbuch (key und value) in der Konsole aus.

Danach:
Mach diese Übungen: https://www.w3schools.com/python/exercise.asp?filename=exercise_dictionaries1

### Advanced-Challenge: Dictionaries

Erstelle ein Programm, das eine Liste von Wörtern einliest und für jedes Wort in der Liste die Anzahl der Vokale zählt. Das Ergebnis sollte als Dictionary ausgegeben werden, wobei die Wörter die Schlüssel und die Anzahl der Vokale die Werte sind.

Beispiel:

Eingabe: ['Apfel', 'Banane', 'Orange']

Ausgabe: {'Apfel': 2, 'Banane': 3, 'Orange': 3}

Das Programm sollte folgende Schritte ausführen:

Definiere eine Funktion, die ein Wort als Eingabe erhält und die Anzahl der Vokale im Wort zählt. Eine Möglichkeit, dies zu tun, besteht darin, eine Schleife zu verwenden, um jedes Zeichen im Wort zu überprüfen, ob es ein Vokal ist.

Definiere eine zweite Funktion, die eine Liste von Wörtern als Eingabe erhält und für jedes Wort die Anzahl der Vokale zählt. Die Funktion sollte ein leeres Dictionary erstellen und für jedes Wort in der Liste die Anzahl der Vokale mithilfe der zuvor definierten Funktion zählen und als Wert im Dictionary speichern.

Schließlich sollte das Programm eine Liste von Wörtern einlesen und die zweite Funktion aufrufen, um das Dictionary mit den Anzahlen der Vokale zu erstellen und auszugeben.

### Challenge 2: Download Youtube - Module importieren

Importiere das Modul `YoutubeDL` aus dem Package `yt_dlp`.
Frage d* Anwender:in nach einem Youtube-Videolink und lade das Video mit `yt_dlp` herunter.

Doku: https://pypi.org/project/yt-dlp/ (see section on [Embedding YT-DLP](https://github.com/yt-dlp/yt-dlp?tab=readme-ov-file#embedding-yt-dlp))

### Challenge 3: PDF Merger - Module importieren

Baue einen PDF Merger mit dem Package `PyPDF2` und dem Module `PdfMerger` (NICHT PdfFileMerger, wie im Link genannt)
https://towardsdatascience.com/merge-pdf-files-using-python-faaeafe4926c

Advanced: Merge nur die erste Seite von 2 mehrseitigen PDFs 
(https://pypdf2.readthedocs.io/en/latest/user/merging-pdfs.html)



## Homework Task

```{note}
**Anmerkung zur Abgabe:**
Erstelle pro Challenge eine eigene Python-Datei. Also z.B.:
`challenge1.py`, `challenge2.py`, `challenge3.py`, und `bonus_challenge.py`
```

### Challenge 1: Steckbriefe deiner Lieblingsserie - Dictionary

Schreibe, analog wie in der Session, 3 Steckbriefe von 3 Charakteren deiner Lieblingsserie.
Die Steckbriefe sollten dabei Name, Alter, Rolle in der Serie und 2 weitere Attribute, die für die Serie relevant sind, enthalten. 
Füge bei einem der 3 Steckbriefe ein Attribut "Bezugspersonen" hinzu. Diese soll eine Liste enthalten, die wiederum für jede Bezugsperson ein `dict` (zumindest mit Namen und Alter) enthält.
Gib dann in deiner Konsole die `values` deiner Steckbriefe in der Konsole aus.
Wenn dir die Informationen fehlen, dann erfinde gerne etwas.

### Challenge 2: Von Schlangen und Schildkröten - Turtle Modules importieren

In dieser Challenge wollen wir mit dem Module `Turtle` arbeiten. 
Wenn wir eine neue Library importieren, ist es immer sinnvoll sich die Doku durchzulesen.
Dort stehen die Funktionalitäten beschrieben. Auf docs.python.org findest du alle nötige Dokumentation. 
Hier das Turtle Module: https://docs.python.org/3/library/turtle.html

Nun die Aufgabe:
Erstelle eine neue Python-Datei und importiere das Turtle-Modul. Erstelle eine neue Turtle-Instanz und verwende die Befehle von Turtle, um ein blumenähnliches Mandala zu zeichnen. Verwende eine For-Schleife, um mehrere Kreise oder Ellipsen  zu zeichnen, um die Strahlen darzustellen. 
Du kannst hierbei unterschiedliche Farben oder Größen verwenden.

Zeichne gerne weitere Formen (du kannst auch die Funktionen `penup` und `pendown` verwenden, um Formen nebeneinander zu zeichnen): https://robertvandeneynde.be/parascolaire/turtle.en.html


### Challenge 3: Finde den Fehler!

In diesen Code haben sich 7 Fehler eingeschlichen, manche davon Syntax-Fehler, manche Logik-Fehler. Finde die Fehler, beseitige sie und kommentiere den Code.

[](guessthenumber_witherrors.py)

### !!!! Bonus-Challenge: Regular Expressions !!!

Schau dir dieses Video zu Regular Expression an:
* Socratica: [Regex](https://www.youtube.com/watch?v=nxjwB8up2gI)

Schreibe eine Regex, die nur Strings akzeptiert, die das Format einer typischen FH-Mailadresse entsprechen:
vorname.nachname@fhwien.ac.at

Teste es mit folgenden Mail-Adressen:
```python
names = ["elka.xharo@fhwien.ac.at","elka.xharo@hotmail.com","jackie@fhwien.ac.at", "jackie.klaura@fhwien.ac.at"]
```