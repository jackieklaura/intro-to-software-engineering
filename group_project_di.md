# Final group project [DI]

Für das abschließende Gruppenprojekt der Gruppe aus dem Digital Innovation Master
werden verschiedene Aspekte des Software Engineering, die wir im Laufe des Kurses
kennengelernt haben, angewendet. Zum einen soll etwas programmiert und anhand eines Auftrags umgesetzt werden. Dabei
soll aber Versionierung mit git eingesetzt und auch ein Code Review Prozess durchlaufen werden. Am Ende wollen wir noch
das aktuelle Buzzword-Thema "AI" und ChatGPT aufgreifen und eine kurze Reflexion darüber anstellen, wie diese den
Softwareentwicklungsprozess unterstützen können.

Während wir in der Session 4 einige Rahmenthemen vorstellen werden, wird das konkret umzusetzende Projekt jeweils in
einem **Auftragsgespräch der einzelnen Lerngruppen mit den LV-Leiterinnen** definiert. Ihr könnt daher auch eigene
Vorschläge formulieren. Neben dem Auftragsgespräch, das online stattfindet, kann jede Lerngruppe im Verlauf des Projekts
noch **ein weiteres online-Meeting mit den LV-Leiterinnen** einfordern. Dieses kann sowohl dazu dienen den konkreten
Auftrag anzupassen, als auch generell Schwierigkeiten bei der Umsetzung zu besprechen.

## Ablauf

Der gesamte Prozess des Gruppenprojekts wird wie folgt ablaufen:

**In der letzten Session bzw. unmittelbar danach:**

1. Ihr startet alle bereits in der letzten Session mit dem **git-howto**. Alles, was sich in der Session nicht mehr ausgeht,
   solltet ihr individuell noch im Rahmen des Gruppenprojekts fertig machen. Daraus ergibt sich zwar kein abzugebender
   code, aber damit seid ihr für den weiteren Prozess der Gruppenarbeit gerüstet.
2. Ebenso noch während der letzten Session bildet ihr **innerhalb eurer Lerngruppen** die **Programming Pairs** und
   brainstormed bereits welches konkrete Projekt ihr umsetzen wollt. Sollten sich in einzelnen Lerngruppen
   zahlenmäßig nicht zwei Paare ergeben, kann entweder eine Person alleine coden, oder eine Person ist Teil von zwei
   Paaren.
3. Nach der letzten Session **definiert ihr die Eckpunkte** eurer konkreten Projekte und kommt damit zu einem der
   anberaumten Auftragsgespräch-Termine.

**Auftragsklärung und Entwicklung:**

4. Sobald der Auftrag geklärt ist, legt ihr für eure Lerngruppe ein **öffentlich einsehbares Repo** an (z.B. auf GitHub),
   mit einer README.md, in der euer Vorhaben beschrieben wird.
5. Für die Entwicklung der **zwei Teilkomponenten** erstellt ihr **zwei branches**, in denen die jeweiligen programming pairs
   unabhängig voneinander ihre Komponenten entwickeln können.

**Review und Analyse:**

6. Ist die Komponente fertig entwickelt, führt das jeweils andere Paar eine **Code Review** für die entwickelte Komponente
   durch. Das Ergebnis der Code Review soll ebenso öffentlich einsehbar festgehalten werden (z.B. auf GitHub als
   Kommentar auf einen Pull-Request, oder als REVIEW.md file im root folder des jeweiligen branches).
   Für das Code Review werden wir entsprechende Leitfäden posten, die ihr für eure Projekte adaptieren könnt. Generell
   könnt ihr euch auch in der Lerngruppe eine eigene Checklist/Guideline für die Code Review überlegen, und diese
   auch in einem File im Repo als Referenz hinterlegen.
7. Nach der Code Review werden deren Ergebnisse von den Teams in ihre Komponenten eingearbeitet und nochmal an die
   Reviewer zurückgegeben. Diese **mergen** dann den branch der Komponente des anderen Paares **nach `main`**.
8. Zum Abschluss sollt ihr in den jeweiligen Programming Pairs **evaluieren** wie euch **ChatGPT** (oder auch eine andere AI)
   bei der Aufgabe unterstützen kann. Welche Teile des Projekts sind in welcher Qualität bereits vorab generierbar?
   Diskutiert eure Erkenntnisse in der Lerngruppe und schreibt als Team eine kurze Analyse dazu. Auch hierzu posten
   wir ein paar Leitfragen in unserem zentralen Kommunikationskanal.
