# Session 5 - API Design & Datenbanken

Für diese Session sind keine extra Vorbereitungsmaterialien zu lesen, außer jene, die bereits für die Homework 4
benötigt werden.

In dieser Session werden wir uns bereits ausführlicher dem Abschlussprojekt widmen.
Zur Vorbereitung schauen wir uns dazu [sqlite3](https://docs.python.org/3/library/sqlite3.html)
und [requests](https://requests.readthedocs.io) an, und werden anhand kleiner Aufgaben erste
Erfahrungen damit sammeln. Danach gehen wir das
[FH Wien Software Engineering Example Project](https://gitlab.com/jackieklaura/fh-wien-example-project)
an, indem eine prototypische Food Sharing Web-Applikation entwickelt wird, wobei
im Backend [Flask](https://flask.palletsprojects.com/) als Framework verwendet wird.

## Session Task

### Challenge 1 - replicate SQL Island

In dieser Challenge werden wir die Daten des [SQL Island](https://sql-island.informatik.uni-kl.de/) Spiels
in einer lokalen SQLite Datenbank mit einem Python script reproduzieren.

1. Starte dazu das Script [sqlite_demo.py](sqlite_demo.py) und schaue dir die damit generierte Datenbank
mit dem [DB Browser for SQLite](https://sqlitebrowser.org/) (aka sqlitebrowser) an.

2. Als nächsten Schritt, sollst du das Script so erweitern, dass auch die beiden anderen Tabellen und ihre
Daten, so wie sie im Spiel vorkamen in die Datenbank initialisiert werden. Dabei kann dir das Script
[sql_island_data.py](sql_island_data.py) helfen, indem die Daten bereits als eine Liste von Tupels vorliegen.

3. Als finalen Schritt ändere die abschließende Abfrage in etwas Spannenderes um (vielleicht erinnerst du
dich von der letzten Homework noch an eine?), oder erstelle überhaupt ein eigenes script _sql_island_queries.py_,
in dem du ein paar spannende Abfragen machen und die resultierenden Daten ausgeben kannst. 

### Challenge 2 - request some weather API data

Verwende die [requests](https://requests.readthedocs.io) library, um Daten von der
[Open-Meteo](https://open-meteo.com/) API abzufragen. Frage zuerst folgenden Endpunkt mit
Parameter für Wien ab, und gib die relevanten Durchschnittswerte aus:
https://api.open-meteo.com/v1/forecast?latitude=48.12&longitude=16.22&current=temperature_2m,wind_speed_10m&hourly=temperature_2m,relative_humidity_2m,wind_speed_10m

Erstelle ein Script, mit dem du diese Daten für eine von zumindest 3 vorkonfigurierten Städten ausgeben kannst.

### Challenge 3 - get the example project running

Nachdem wir das [FH Wien Software Engineering Example Project](https://gitlab.com/jackieklaura/fh-wien-example-project)
gemeinsam durchgegangen sind, ist es nun eure Aufgabe, dieses Repo zu Klonen und das Backend sowie zumindest ein
Frontend zum Laufen zu bekommen.
