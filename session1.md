# Session 1 - Input, String Manipulation & If/else

## Materialien zur Vorbereitung

Folgende Materialien könnten hilfreich sein:

* Mike Dane: [Mad Libs](https://www.mikedane.com/programming-languages/python/building-a-mad-libs-game/) (Video, 5 min)
* freeCodeCamp.org: [Mad Libs](https://www.youtube.com/watch?v=8ext9G7xspg&t=100s) (Video, ~8 min)
* PythonForBeginners: [String manipulation](https://www.pythonforbeginners.com/basics/string-manipulation-in-python#htoc-replace-substring-in-a-string-in-python)
* W3Schools: [Python Tutorial](https://www.w3schools.com/python/default.asp)
* [devdocs.io](https://devdocs.io/) (Python aktiveren und nicht benötigte Sprachen deaktivieren)
* Real Python: [Python String Formatting Best Practices](https://realpython.com/python-string-formatting/)


## Session Task

### Hello World

Die Mutter aller Programmierbefehle zeigt schon sehr deutlich wie einfach Python ist.
Gib dafür einfach ein:
 
```python
print("Hello World")
```

### Challenge 1: Pensionsrechner

Schreibe ein Skript mit der Variable `age`. Schreibe dort dein Alter hinein.
Berechne in einer anderen Variable `pension`, wieviele Wochen du noch bis zur Pension arbeiten musst (nimm 65 Jahre als Pensionsantrittalter).

### Challenge 2: Wie lange musst du arbeiten, um soviel Geld wie Elon Musk zu haben

Schreibe ein Skript mit der Variable `yearlyincome`, wo du dein oder das durchschnittliche Jahresgehalt (ca. 37000 Euro) in Österreich einträgst.
Berechne jetzt in einer neuen Variable, wieviele Jahre du arbeiten müsstest, um 150 Mrd Euro Vermögen anzuhäufen (Stand Musk Vermögen, Jänner 2023).


### Challenge 3: Chatbot

Schreibe ein Skript das dich nach deinem Namen fragt und dich dann grüßt.
Dann soll es nach deinem Alter fragen und es dir zurück ausgeben.

Output zB:
```
Hi, wie heißt du?
>> Nala
Hallo Nala. Wie alt bist du?
>> 30
Wow Nala, du siehst garnicht aus wie 30.
```

### Challenge 4: Message of the day – else if

Schreibe ein Skript, das den/die User*in nach dem Wochentag fragt (Zahl von 1 bis 7 oder String).
Je nach Wochentag soll eine andere Message of the day ausgegeben werden.
Baue den Inputstring in die Ausgabe mit ein (String concatenation).
Am Wochenende soll „Have fun“ ausgegeben werden.
Berücksichtige auch den Fall einer ungültigen Eingabe.


### Challenge 5: Intelligenter Chatbot - If/else

Entwickle den Chatbot weiter und baue mit if/else mehrere Gesprächsfäden.
Lass deinen Bot zb fragen, wie es dir geht auf einer Skala von 1-10.
Wenn der Input kleiner als 5 ist gib eine andere Nachricht aus, als wenn die Zahl über 5 ist.

Output zB:
```
Wie geht es dir (auf einer Skala von 1-10, 10 is das höchste)?
>> 3
Oh, das tut mir leid. Vielleicht heitert dich unser Gespräch auf.

Wie geht es dir (auf einer Skala von 1-10, 10 is das höchste)?
>> 8
Das ist ja toll. Mir geht es auch gut.
```

Baue noch mehrere Fragen mit if/else ein. Vergleiche auch input Strings.
Wenn du zufrieden bist, dann probier den Chatbot deiner\*s Sitznachbar*in aus.


## Homework Task

### Challenge 1: Demotivational  quotes - Mad Libs

Erstelle einen "Demotivational Quotes"-Generator nach dem Mad Libs Prinzip.
Mad Libs ist ein Spiel, das wie ein Lückentext aufgebaut ist. Zuerst werden von den Spieler*innen mehrere Wörter als Eingabe verlangt (ohne dass sie den Lückentext kennen). Dann werden diese Wörter in den Text eingefügt, was zu sehr eigenartigen Kombinationen führt.
Info: Du kannst entweder alles in Englisch oder alles in Deutsch schreiben. 

* Suche dir dafür eine möglichst lange Motivational Quote oder dein Lieblingszitat raus (zb hier: https://blog.hubspot.com/sales/famous-quotes).

* Erstelle die Datei `madlibs.py`

* Lege für jedes Wort, das du im Text austauschen willst, eine Variable an und triggere mit `input()` eine User-Eingabe.
Zb.:

```python
verb = input("Name a verb: ")
tier = input("Your favourite animal: ")
```

* Gib den Lückentext mit dem `print()` Befehl aus. 

* Kommentiere deinen Code sinnvoll.

* Probier das Mad Libs Spiel einer/eines Kolleg*in aus und lass sie/ihn das Ergebnis als Kommentar notieren.



### Challenge 2: Choose your quote - if/else

Wir wollen Task 1 jetzt erweitern und dem/der User*in die Möglichkeit geben zwischen mehreren Quotes zu wählen.

* Kopiere den Code aus Task 1 dafür in eine neue Datei `chooseyourquote.py`

* Frage den/die User\*in, ob er/sie lieber eine mad lib quote (1), einen echten Motivationsspruch (2) oder ein philosophisches Zitat (3) haben möchte. Der/die User*in soll mit der Eingabe von 1, 2 oder 3 den Wunsch eingeben.

* Baue nun eine if/else-Abfrage, wo im Falle von "1" der Code aus Task 1 ausgeführt wird, im Falle von "2" der Originalspruch und im Falle von "3" ein philosophischer Spruch deiner Wahl mit dem `print()` - Befehl ausgegeben wird.

* Kommentiere deinen Code sinnvoll.


### Challenge 3: Let me fix this - String manipulation against Schwurbler

Wir wollen in diesem Task ein Tweet von dem berühmt-berüchtigten Corona-Leugner Attila Hildmann mit String manipulation "korrigieren". 
(Quelle: https://www.gq-magazin.de/entertainment/artikel/attila-hildmann-verschwoerungstheorien-werden-immer-abstruser)
Info: Alle Befehle dafür findest du auf der oben verlinkten PythonForBeginners Website.

Folgender String:
```
attila = "DARUM GEHT ES BEI CORONA! EUER EGO SOLL IN EINE CLOUD HOCHGELADEN WERDEN, DIE LEITUNG IN EUREN KÖRPER WIRD GELEGT DURCH EINE IMPFUNG UND DIE DARIN ENTHALTENE NANOTECHNOLOGIE STELLT DIE VERBINDUNG HER ZUR CLOUD! IHR SOLLT UNGEFRAGT UNSTERBLICH WERDEN, DAMIT IHR IN DIESER MATERIELLEN WELT VERFANGEN BLEIBT UND NIEMALS EUREN SEELENFRIEDEN BEKOMMEN KÖNNT, NIEMALS ZU GOTT KOMMEN KÖNNT! DAS IST SATANS PLAN!"
```

* Erstelle dafür eine neue Datei `letmefixthis.py` mit der Variable 'attila'.

* Zuerst wollen wir nicht angeschrien werden, ergo den String in Kleinbuchstaben umwandeln.

* Dann benutze den `replace()`-Befehl um folgende Wörter auszutauschen:

    * corona -> deine Lieblingsserie
    * ego -> das Letzte, was du gegessen hast
    * eine impfung -> deine Lieblingsmusik
    * nanotechnologie -> ein Instrument
    * seelenfrieden -> ein Preis (zB Oscar, Golden Globe,...)
    * satans -> Märchenfigur 

  Sei kreativ und benutz gerne ganz andere Austauschwörter. Gib das Ergebnis mit einem `print()` aus.

* Zähle wie oft der Anfangsbuchstabe deines Namens im Text vorkommt. Gib das Ergebnis mit einem `print()` aus.

* Um Satans Plan zu vollenden, gib den String in umgekehrter Reihenfolge aus (`reversed()` und `join()`).

* Zum Schluss, wollen wir den Tweet in seine einzelnen Sätze aufspalten. Benutze dafür die Funktion `split()`. Benutze dafür das Trennzeichen '!'.
  Gib das Ergebnis aus und schau dir an, welcher Datentyp das sein könnte.

* Kommentiere deinen Code sinnvoll.

### !!! Bonus-Challenge !!!

Um Bonuspunkte zu sammeln, baue dein eigenes "Let me fix this" - Skript.
Suche ein Zitat, das du "verschönern" willst und führe zumindest 4 (zumindest teilweise andere) String Manipulation Funktionen aus.