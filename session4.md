# Session 4 - Software Engineering Crash Course

Verwende eine Auswahl aus folgenden Materialien, um dich vorab auf diese Session vorzubereiten. Für die Vorbereitung
im Selbststudium sind für diese Session durchschnittlich 4 Stunden anberaumt.

* Software Engineering
  * Mindestens die ersten drei Abschnitte von [Wikpedia (en): Software engineering](https://en.wikipedia.org/wiki/Software_engineering) (~15 min Text)
  * [Wikipedia (de): Vorgehensmodell zur Softwareentwicklung](https://de.wikipedia.org/wiki/Vorgehensmodell_zur_Softwareentwicklung) (~10 min Text)
  * CodeLandConf 2022: [Ramón Huidobro: There's More to Open Source than Code](https://community.codenewbie.org/codeland2022/on-demand-talk-theres-more-to-open-source-than-code-1ne8) (~15 min Video)
* Code Review 
  * [Wikipedia (en): Code review](https://en.wikipedia.org/wiki/Code_review) (~8 min Text) 
  * FreeCodeCamp: [Jean-Charles Fabre: A zen manifesto for effective code reviews](https://www.freecodecamp.org/news/a-zen-manifesto-for-effective-code-reviews-e30b5c95204a/) (~16 min Text)
  * FreeCodeCamp: [Assaf Elovic: Code Review — The Ultimate Guide](https://www.freecodecamp.org/news/code-review-the-ultimate-guide-aa45c358bbf5/) (~10 min Text)
  * CodeLandConf 2022: [Angie Jones: Ten Commandments of Navigating Code Reviews](https://community.codenewbie.org/codeland2022/keynote-ten-commandments-of-navigating-code-reviews-with-angie-jones-1b05) (~45 min Video)
* Version Control
  * FreeCodeCamp: [Gwendolyn Faraday: Git and GitHub for Beginners - Crash Course](https://www.youtube.com/watch?v=RGOj5yH7evk) (~1h 10min Video) 
  * [Vincent Driessen: A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) (~30 min Text)
* Scrum & Agile
  * [Scrum Alliance: What is Scrum?](https://www.youtube.com/watch?v=TRcReyRYIMg) (1 min 20 sec Video)
  * FreeCodeCamp: [Adam Naor: Agile Methods and Methodology for Beginners – Agile Software Development and Agile Project Management Examples](https://www.freecodecamp.org/news/agile-methods-and-methodology-for-beginners/) (~20 min Text)


## Session Task

### Challenge: Getting down with git

In dieser Challenge ist das [GitHowTo](https://githowto.com) von Alexander Shvets in mehreren Schritten durchzuarbeiten:

* In einer ersten Sequenz sollen die Abschnitte 1 bis 16 bearbeitet werden
* Nach einer Zwischenbesprechung werden die Abschnitte 17 bis 33 bearbeitet.
* Die restlichen Abschnitte 34 bis 51 sind als Teil des Gruppenprojekts im Selbsstudium durchzuarbeiten.

### Bonus Challenge: Merging the branch out of this commit!

Für alle besonders Schnellen oder Neugierigen bietet sich as interaktive und (mit Graphen) visualisierte
[Learn Git Branching](https://learngitbranching.js.org/) Tutorial an, um sich ein besseres
Verständnis dessen anzueignen, was eigentlich passiert, wenn `branch`es erstellt oder ge`merge`d
werden, oder wenn auch nur einzelne commits `rebase`d, oder gar nur Teile eines commits ge`cherry-pick`ed
werden.


## Homework Task

### Challenge 1: Python Basics - Lottozahlen (10 Punkte)

Schreibe eine Funktion `lottozahlen`, welche die Ziehung von Lottozahlen simuliert.
Dabei werden **6 eindeutige Zufallszahlen** aus dem Bereich **1 bis 45** gezogen.

Verwende dazu eine **For-Schleife**, um die Zahlen zu generieren. Nach **jeder Ziehung**
soll die aktuelle Nummer der Zahl und die gezogene Lottozahl ausgegeben werden. Nutze
`time.sleep(1)` um nach jeder Zahl eine kurze Verzögerung einzubauen. Achte darauf,
dass keine Zahl doppelt gezogen wird.

Erstelle außerdem ein dictionary mit witzigen Assoziationen zu einigen Zahlen, z.B.:
```python
beschreibungen = {
  '7': 'James Bond',
  '13': 'Freitag der 13.',
  '33': 'Alter Jesu',
  # ...
}
```

Bei jeder Ausgabe, also jedem Schleifendurchgang, prüfe ob die Zahl eine Beschreibung
hat und gib diese aus.

**Beispielausgabe:**
```
1. Zahl: 33 (Alter Jesu)
2. Zahl: 14
   ....
```

Mach das Programm **interaktiv** und gib dem/der User*in die Möglichkeit **eigene Beschreibungen**
hinzuzufügen. Schreibe die **Funktion** `ask_user` und frag mit (mehreren) `input`-Befehlen zuerst
nach der Zahl und nach der Beschreibung die hinzugefügt werden soll. Füge diese zum **Dictionary**
hinzu.

**Beispielstruktur:**
```python
def lottozahlen():
    # do something

def ask_user():
    # do something


# Programm ausführen
if __name__ == "__main__":
    ask_user()
    lottozahlen()
```

```{note}
**Wichtiger Hinweis:**<br>
Schreibe als erstes in Stichwörtern auf, welche Funktionalitäten du als erstes
umsetzen möchtest. **Schreibe das bitte als Kommentar oben in dein Skript!**
Teile das Problem zuerst immer in kleinere Probleme auf! Beginne mit einem kleinen
funktionierenden Programm, das erst einen Teil der Funktionalitäten beinhaltet.
Wenn das funktioniert, gehe zum nächsten Teilaspekt.
```

### Challenge 2: SQL Basics (10 Punkte)

**SQL Intro**

Mach dich zuerst mit diesem SQL-Tutorial vertraut. Schau dir dafür folgende Unterseiten an:
home, intro, syntax, select, where, order by, and, or, update, count, wildcards, joins
* https://www.w3schools.com/sql/sql_syntax.asp

**SQL Island**

Dann spiele dieses lustige SQL Tutorial https://sql-island.informatik.uni-kl.de/ bis zu
dem Punkt in folgendem Screenshot durch. Mach dann einen Screenshot von deinem letzten Schritt
und lade ihn hoch.

![](./img/s4-hw2-img1.png)

**Eigene Aufgabe:**

Auf der Website vom SQL Island findest du im Menü den “Sandbox Modus”. Nutze diesen,
um folgende Aufgaben zu lösen. Kopiere die Abfragen, dann in eine Textdatei und lade sie hoch.

**Fragestellung 1:** Finde alle Bewohner*innen, deren Namen auf "ochse" enden.

![](./img/s4-hw2-img2.jpg)

**Fragestellung 2:** Zähle die Anzahl der männlichen Bewohner in der Tabelle BEWOHNER

![](./img/s4-hw2-img3.jpg)

**Fragestellung 3:** Namen der Bewohner*innen und die Gegenstände, die sie besitzen.

![](./img/s4-hw2-img4.jpg)

### Challenge 3: Web Application Basics (5 Punkte)

In dieser Aufgabe geht es darum, dich in einem ersten Schritt mit Web-Applikationen
und ihren unterschiedlichen Komponenten vertraut zu machen, in dem du folgende zwei
Texte liest und danach (es macht danach viel mehr Sinn) noch das als drittes Item
gelistete Video anschaust:

* jackie: [Web Application Security: hands-on intro](https://hackmd.io/@jackie/Hthootw-Intro) (~30min read @ HackMD)
* Dionysia Lemonaki : [Frontend VS Backend – What's the Difference?](https://www.freecodecamp.org/news/frontend-vs-backend-whats-the-difference/) (30min read @ FreeCodeCamp)
* IBM Technoogy / Nathan Hekman: [What is a REST API?](https://www.youtube.com/watch?v=lsMQRaeKNDk) (~9min watch @ YouTube)

Beantworte dann folgende Fragen in einem Textdokument mit einer Länge von einer halben
bis maximal einer ganzen Seite (und gib es dann als PDF ab):
* Was bedeutet für dich Frontend?
* Was bedeutet für dich Backend?
* Wie klar kannst du dir die Kommunikation zwischen Backend und Frontend vorstellen?
  Hast du auch schon von anderen Methoden dafür außer REST APIs gehört?
* In Zusammenschau mit der SQL-Aufgabe: findest du das Datenbank-Design soll den
  Backend-Entwickler:innen überlassen werden, oder braucht es dafür eigene
  Datenbank-Spezialist:innen? Oder sollen hier gar auch Frontend-Leute
  oder andere Stakeholder:innen involviert sein?

```{note}
Es geht hier nicht um richtige Antworten, sondern darum wie du selbst es verstehst.
Also einfach in eigenen Worten ausdrücken, auch wenn's vielleicht mal nicht so abgerundet wirkt.
Die Punkte gibt es nur dafür, ob die Seite abgegeben wurde und du dich mit dem Thema
auseinandergesetzt hast - nicht dafür ob du hier richtige Antworten gibst oder den
Themenbereich bereits voll überblickst und verstanden hast.
```
