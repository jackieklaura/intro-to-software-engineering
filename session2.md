# Session 2 - Loops & Functions

## Materialien zur Vorbereitung

Verwende eine Auswahl aus folgenden Materialien, um dich vorab auf diese Session vorzubereiten. Für die Vorbereitung
im Selbststudium sind für diese Session durchschnittlich 4 Stunden anberaumt.

* Lists:
    * Automate The Boring Stuff With Python: [Chapter 4: Lists](https://automatetheboringstuff.com/2e/chapter4/) (~55 min Text)
    * FreeCodeCamp: [Python Lists](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/python-lists) (~11 min Video)
    * FreeCodeCamp: [Working with Lists](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/working-with-lists) (~10 min Video)
    * FreeCodeCamp: [Strings and Lists](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/strings-and-lists) (~8 min Video)
* Loops:
    * Automate The Boring Stuff With Python: [Chapter 2: Flow Control](https://automatetheboringstuff.com/2e/chapter2/) (~45 min Text) 
    * FreeCodeCamp: [Loops and Interation](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/loops-and-iterations) (~10 min Video)
    * FreeCodeCamp: [Iteration: Definite Loops](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/iterations-definite-loops) (~7 min Video)
    * FreeCodeCamp: [Iteration: Loop Idioms](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/iterations-loop-idioms) (~9 min Video)
    * FreeCodeCamp: [Iteration: More Patterns](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/iterations-more-patterns) (~19 min Video)
* Funktionen:
    * Automate The Boring Stuff With Python: [Chapter 3: Functions](https://automatetheboringstuff.com/2e/chapter3/) (~35 min Text) 
    * FreeCodeCamp: [Python functions](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/python-functions) (~11 min Video)
    * FreeCodeCamp: [Build your own Functions](https://www.freecodecamp.org/learn/python-for-everybody/python-for-everybody/build-your-own-functions) (~13 min Video)
    * PyLadies Vienna: [Functions](https://pyladies.at/2022/pyladies-en-vienna-2022-autumn/beginners-en/functions/) (~10 min Text)
    * PyLadies Vienna: [Custom Functions](https://pyladies.at/2022/pyladies-en-vienna-2022-autumn/beginners-en/def/) (~10 min Text)

## Screencasts zum Nachschauen und Vertiefen

* Session Tasks:
    * [Challenge 3 – Random Friend Finder (quick & dirty version)](https://nextcloud.tantemalkah.at/index.php/s/pX65KrJ4aNHGeXc) (~30min/100MB)
    * [Challenge 3 – Random Friend Finder (improved version)](https://nextcloud.tantemalkah.at/index.php/s/BAgtx5YSxmiYT4p) (~45min/170MB)
* [Rekursionen & Debugger](https://nextcloud.tantemalkah.at/index.php/s/BfqZ26jF9onFEC3) (~40min/100MB)
* [Testen mit pytest](https://nextcloud.tantemalkah.at/index.php/s/A7bkLfzbDrzDkjk) (~7min/25MB)

## Ergänzungsmaterial für nach der Session

Hier einige erklärende Ressourcen für jene Dinge, für die in der Session nicht so viel Zeit blieb:

* Debugger:
  * Real Python on YouTube: [Step Through Python Scripts With Thonny](https://www.youtube.com/watch?v=QQAqQp06nXo) (~6 min Video)
  * Gary Marrer on YouTube: [Using the Debugger in Thonny](https://www.youtube.com/watch?v=6L72Kqw2TnQ) (~10 min Video)
    * basiert auf einer älteren Thonny-Version, in der Breakpoints noch nicht verfügbar waren
* Rekursion:
  * Recording von Jackie zu: [Rekursionen](https://nextcloud.tantemalkah.at/index.php/s/BfqZ26jF9onFEC3) (~39min Video) - dabei wird auch der Debugger mit Breakpoints verwendet
* Testen:
  * Recording von Jackie zu: [Testen](https://nextcloud.tantemalkah.at/index.php/s/A7bkLfzbDrzDkjk) (~7min Video)

## Code snippets in session

### Einfache `while` Schleifen

```python
number = 1

while number < 100:
	print(f'counting from {number} to {number+1}')
	# and maybe doing some more serious stuff here
	number += 1

# Thanks to Al Sweigart for the following one
name = ''
while name != 'your name':
	print('Please type your name.')
	name = input()
print('Thank you!')
```

### Mit `continue` und `break` die Schleife steuern

```python
boredom_threshold = 42  
  
number = 0  
while number < 100:  
    # increase the loop counter  
    number += 1  
    # we only want to deal with even numbers, so we just skip odd ones  
    if number % 2 != 0:  
        continue  
    # and in case we get past our boredom threshold, we just end the loop  
    if number > boredom_threshold:  
        break  
    print(f'we have reached {number}')  
    # and maybe do some more important stuff here as well
```

### Lists (and strings) : is there anything they can't do?

```python
my_list = ['apple', 'banana', 'chocolate chip', 'cherry', 'cola']
c_foods = []

# now let's loop through our list and do something with every item in it
for item in my_list:
	# check whether it starts with a 'c' and add it to our c_foods list
	if item.startswith('c'):
		c_foods.append(item)

# use the nifty str.join function to combine all c_foods into a string
i_likes = ', and '.join(c_foods)
print(f'I really likes {i_likes} a lot!')

# also check out all the awesome string functions, especially split and strip
```

* W3Schools: [Python Tutorial: String Methods](https://www.w3schools.com/python/python_strings_methods.asp)

### Rekursion anhand der Fibonacci-Folge

```python
fibonacci = [0, 1, 1, 2, 3, 5, 8, 13, 21]  
for _n in range(100):  
    fibonacci.append(fibonacci[-1] + fibonacci[-2])  
print(fibonacci)  
  
  
def fibonacci_via_list(n):  
    if n <= 1:  
        return n  
    f_list = [0, 1]  
    for _i in range(2, n+1):  
        f_list.append(f_list[-1] + f_list[-2])  
    return f_list[-1]  
  
  
def fibonacci_recursive(n):  
    if n <= 1:  
        return n  
    return fibonacci_recursive(n-1) + fibonacci_recursive(n-2)  
  
  
for i in range(20):  
    print(f'fibonacci_recursive({i}) : {fibonacci_recursive(i)}')  
    print(f'factorial_via_list({i})  : {fibonacci_via_list(i)}')
```

## Session Task

### Challenge 1 : THE Answer guessing game

Schreibe ein Skript, das in einem `while`-Loop die/den Benutzer\*in dazu auffordert DIE Antwort einzugeben, solange
bis die richtige Antwort (z.B. '42') eingegeben wurde. Sollte aber 'let me out' eingegeben werden, verwende ein `break`
um aus dem `while`-Loop auszubrechen.

### Challenge 2 : Analysing and scrambling the Zen of Python structure

Nimm folgende Liste als Basis für ein neues Skript:

```python
zen_of_python = [
    'Beautiful is better than ugly.',
    'Explicit is better than implicit.',
    'Simple is better than complex.',
    'Complex is better than complicated.',
    'Flat is better than nested.',
    'Sparse is better than dense.',
    'Readability counts.',
    'Special cases aren\'t special enough to break the rules.',
    'Although practicality beats purity.',
    'Errors should never pass silently.',
    'Unless explicitly silenced.',
    'In the face of ambiguity, refuse the temptation to guess.',
    'There should be one-- and preferably only one --obvious way to do it.',
    'Although that way may not be obvious at first unless you\'re Dutch.',
    'Now is better than never.',
    'Although never is often better than *right* now.',
    'If the implementation is hard to explain, it\'s a bad idea.',
    'If the implementation is easy to explain, it may be a good idea.',
    'Namespaces are one honking great idea -- let\'s do more of those!',
]
```

Erstelle eine leere Liste, an die wir im Laufe des Skripts noch Dinge anhängen können.
Verwende einen `for`-Loop um nun durch die `zen_of_python` Liste zu gehen. Gib für jeden Satz die Anzahl an Wörter aus.
Füge außerdem das erste und das letzte Wort zur initial leeren Liste hinzu. Achte dabei darauf, allfällige Satzzeichen
am Ende des Worts zu entfernen.

Nachdem der Loop beendet ist, gib alle in der Liste gesammelten Wörter durch den string `' is '` verbunden aus.

### Challenge 3 : Random Friend Reminder

Für diese Challenge sind mehrere Funktionen zu schreiben:

**Eine Funktion namens `print_greeting` mit drei Parametern:**

- der erste fixe Parameter ist der Name einer zu grüßenden Person.
- die weiteren optionalen Parameter sind:
  - die Tageszeit ("Morgen", "Tag", oder "Abend"), wobei der default "Tag" sein soll
  - ob eine förmliche oder eine informelle Anrede gewählt werden soll (default: informell)

Diese Funktion gibt eine entsprechende Grußmeldung an die/den Benutzer\*in aus. Z.b. könnte dabei folgende Ausgaben
produziert werden:

* Willkommen Alice, einen schönen guten Morgen wünsche ich Dir.
* Willkommen Bob, einen schönen guten Tag wünsche ich Dir.
* Willkommen Eve, einen schönen guten Abend wünsche ich Ihnen.

**Eine Funktion `get_name`:**

Diese fragt die/den Benutzer\*in nach deren Namen gibt diesen als string zurück

**Eine Funktion `get_friends`:**

Diese fragt die/den Benutzer\*in so lange nach Namen von Freund\*innen fragt, bis eine leere Zeile bzw. ein leerer
string eingegeben wird. Diese Funktion gibt schließlich eine Liste mit allen eingegebenen Namen zurück.

**Eine Funktion namens `init`:**

Diese ruft zuerst die `get_name` Funktion auf, um einen Namen abzufragen.
Daraufhin wird die `print_greeting` Funktion verwendet, um einen entsprechenden Gruß auszugeben. Verwende dabei die
aktuelle Uhrzeit anhand folgendem Schema, um die Tageszeit und die Förmlichkeit der Anrede zu gestalten:

- Vor 10 Uhr: "Morgen" und informell
- Vor 17 Uhr: "Tag" und formell
- Ab 17 Uhr: "Abend" und informell

Die Stunde der aktuellen Uhrzeit kann mit `datetime.now().hour` ermittelt werden. Dazu muss `datetime` über folgende
Zeile am Beginn des Skripts importiert werden:

```python
from datetime import datetime
```

Nachdem der Gruß ausgegeben wurde, soll die `get_friends` Funktion verwendet werden, um eine Liste von Freund\*innen
abzufragen. Das Ergebnis wird schließlich verwendet um eine\*n zufällige\*n Freund\*in auszuwählen und z.B. folgenden
Satz auszugeben `f'Hey, wie wärs mal wenn du {friend_name} mal wieder fragst ob ihr euch mal treffen wollt.'`.
Um eine Zufallszahl für den Index in der Liste zu generieren, verwende die `randint` Funktion.
Ref: [W3Schools: Python Random randint() Method](https://www.w3schools.com/python/ref_random_randint.asp)

Verwende schließlich am Ende des Skripts folgenden Code-Block um die `init` Funktion dann auszuführen, wenn das Skript
wirklich als eigenständiges Skript aufgerufen wird:

```python
if __name__ == '__main__':
    init()
```

Wenn Zeit bleibt, versuche im Debugger die Ausführung deines Programms zu verfolgen. Kannst du die Reihenfolge, in der
du die Funktionen schreibst, beliebig ändern?

## Homework Task

### Factorial recursion

In diesem Task geht es darum, die [Fakultät](https://de.wikipedia.org/wiki/Fakult%C3%A4t_(Mathematik)) (bzw. Faktorielle,
im Englischen "factorial") einer Zahl zu berechnen. Dabei soll das Prinzip der Rekursion verwendet werden.

Die Aufgabe ist nun: schreibe ein Skript namens `factorial.py` mit einer Funktion `factorial`, die einen Parameter
annimmt: die Zahl, für welche die Fakultät berechnet werden soll. Der Rückgabewert der Funktion ist die Fakultät
dieser Zahl. Die Funktion soll dabei rekursiv sich selbst aufrufen, um die Fakultät der um eins niedrigeren Zahl
zu berechnen.

### FizzBuzz

Erstelle ein Skript mit dem Namen `fizzbuzz.py`. Erstelle darin eine Funktion namens `fizzbuzzer`, die eine Nummer als
Argument annimmt. Die Funktion soll dann entweder die Nummer selbst wieder retournieren, oder einen der folgenden
strings, abhängig vom input:

* ‘fizz’ wenn die Nummer durch 3 teilbar ist
* ‘buzz’ wenn die Nummer durch 5 teilbar ist
* ‘fizzbuzz’ wenn die Nummer durch 3 und 5 teilbar ist

Erstelle nun eine weitere Funktion names `fizzbuzz_debug`, die eine Schleife von 1 bis 100 startet und das Ergebnis
der `fizzbuzzer` Funktion für alle Nummern (von 1 bis 100) auf der Konsole ausgibt.

> **DRY note:** Diese neue Funktion soll die andere schon bestehende Funktion aufrufen. Vermeide es den Code, der den
> Test auf ein fizz, buzz oder fizzbuzz ausführt zu replizieren / aus dem `fizzbuzzer` zu kopieren. Rufe stattdessen
> in der `fuzzbuzz_debug` den `fizzbuzzer` auf und verwende dessen Ergebnis. Eine generelle Regel bzw. best practice
> für Softwareentwicklung ist: keep your code DRY. Das bedeutet: Do not Repeat Yourself.

Schreibe eine zusätzliche Funktion `fizzbuzz_counter`, die auch von 1 bis 100 zählt und das Ergebnis des `fizzbuzzer`
ausgibt, allerdings eine Verzögerung zwischen jeder Ausgabe einfügt (z.B. 0,5 oder 1 Sekunde). Verwende dazu
`from time import sleep` am Anfang des Skripts, um die `sleep()`-Funktion aufrufen zu können.
Library docs: [time.sleep()](https://docs.python.org/3/library/time.html#time.sleep)

Das Skript soll im Normalfall nur den Zeit-verzögerten `fizzbuzz_counter` aufrufen, wenn es ohne weitere Argumente
ausgeführt wird (z.B. `python3 fizzbuzz.py`). Wenn allerdings ein `nodelay` Argument beim Aufruf des Skripts angegeben
wird (z.B. `python3 fizzbuzz.py nodelay`), dann soll das Skript alternativ nur die `fizzbuzz_debug` Funktion ohne
Verzögerung aufrufen. Verwende `sys.argv` um zu überprüfen, ob und welches Argument übergeben wurde.
Library docs: [sys.argv](https://docs.python.org/3/library/sys.html#sys.argv)
PythonForBeginners.com: [How to Use sys.argv in Python?](https://www.pythonforbeginners.com/system/python-sys-argv)

Wenn das erste Argument des Skripts `list` ist, dann soll das Skript keinen der beiden Fizz-Buzz Counters verwenden,
sondern eine `list` generieren, welche die Ergebnisse des `fizzbuzzer` für alle geraden Zahlen zwischen 1 und 50
(inklusive) beinhaltet. Von dieser Liste soll dann ein String generiert werden, der folgende Trennzeichen zwischen
den einzelnen Listenelementen verwendet, abhängig vom zweiten Argument, das dem Skript übergeben wurde:

* ' - ' wenn das zweite Argument `hyphenate` ist
* ' ' wenn das zweite Argument `space` ist
* ', ' in allen anderen Fällen (z.B. wenn es kein zweites Argument gibt)

Dieser neu generierte String soll dann auf der Konsole ausgegeben werden.

### Personal Bonus Challenge: Output-Verschönerung

Für diese bonus challenge erstelle bitte eine **Kopie des `fizzbuzz.py` Files nach `fizzbuzz_bonus.py` und arbeite dann
mit dieser Kopie weiter**. Die später erwähnten Tests werden nämlich nur das fizzbuzz.py ohne den veränderten Output der
Bonus Challenge testen.

Erstelle zusätzliche Funktionen namens `print_fizz`, `print_buzz` und `print_fizzbuzz`, die ein einzelnes 'fizz', 'buzz'
oder 'fizzbuzz' ausgeben. Dabei könnten ASCII art oder ASCII banners zum Einsatz kommen, sodass ein 'fizz' z.B. so
ausschauen könnte:

```
#######   ###   ####### #######
#          #         #       #
#          #        #       #
#####      #       #       #
#          #      #       #
#          #     #       #
#         ###   ####### #######
```

Oder es wird ein Befehl wie `cowsay` verwendet, um ein fizzbuzz wie folgendes zu generieren:

```
 __________
< FizzBuzz >
 ----------
        \   ^__^
         \  (oo)\_______
            (__)\       )\/\
                ||----w |
                ||     ||
```

Nun soll in der `fizzbuzz_counter` Funktion, immer wenn keine Nummer, sondern ein fizz, buzz oder fizzbuzz ausgegeben
werden soll eine der drei neuen Funktionen verwendet werden, anstatt nur den Rückgabewert selbst mit `print` auszugeben.

Verwende außerdem die `os.system()` Funktion um den Systembefehl `clear` (Linux/Unix) bzw. `cls` (Windows) immer direkt
vor dem eigentlichen print auszuführen. Library docs: [os.system()](https://docs.python.org/3/library/os.html#os.system)

### Testen der Abgabe

Um alle Punkte für diese Übung zu erhalten, muss der abgegebene Code einige Tests bestehen. Diese Tests können selbst
auch vor der Abgabe durchgeführt werden, um den eigenen Code zu überprüfen. Dafür wird das `pytest` Modul benötigt,
das im Thonny über das Menü bei den Tools und "manage packages" installiert werden kann. In einer klassischen standalone
Python Installation, würde mensch das mit `pip install pytest` machen (vorausgesetzt `pip` wurde bereits installiert.)

Um die Tests nun durchzuführen, müssen die Test-Files
[test_fizzbuzz.py](test_fizzbuzz.py) sowie [test_factorial.py](test_factorial.py) heruntergeladen und im Folder
gespeichert werden, in dem sich auch die zu testenden Skripte befinden. Danach wird folgender Befehl verwendet,
um die Tests laufen zu lassen (dazu könnt ihr in Thonny im Tools-Menü eine System Shell öffnen):

```shell
python -m pytest -v
```

**Hinweis zu Python-Versionen:** bei einer Thonny-Installation ohne weitere Python Installationen ist das Python binary
als `python` aufzurufen. In komplexeren Setups muss aber mitunter zwischen unterschiedlichen Python Versionen
unterschieden werden, oft ist es dann notwendig `python3` aufzurufen (insbesondere unter Linux und Mac OS X).
Je nachdem, ob `python` als Befehl vorhanden ist, kann es sein, dass manche der Tests nicht durchlaufen, da sie
`python` als Systembefehl ausführen. Solltet ihr also nur `python3` zur Verfügung haben könnt ihr entweder dem
Ordner wo `python3` liegt einen Link darauf namens `python` erstellen, oder ihr schreibt im Testfile die jeweiligen
`python` Vorkommnisse in `python3` um.