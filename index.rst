Willkommen zur Doku für die LV Introduction to / Agile Software Engineering
===========================================================================

Diese ist die kombinierte Dokumentation zu den Lehrveranstaltungen *Introduction to Software Engineering*
des `Digital Innovation <https://www.fh-wien.ac.at/studium/master/digital-innovation/>`_ Masterstudiums und
*Agile Software Engineering* des `Digital Technology & Innovation <https://www.fh-wien.ac.at/studium/master/digital-technology-and-innovation/>`_
Masterstudiums an der FHWien der WKW.

In diesen Kursen werden die Grundlagen der Programmiersprache Python vermittelt sowie ein Einblick in
Software-Entwicklungsprozesse. Für die Teilnehmer:innen der *Introduction to Software Engineering* startet der
Kurs bezüglich Programmierkenntnissen von Null, es sind daher keine Vorkenntnisse notwendig. Nach 4 Session schließen
die Teilnehmer:innen dann mit einem kleinen prototypischen Programmierprojekt ab. Die Teilnehmer:innen des
*Agile Software Engineering* steigen bereits mit Vorkenntnissen zur Session 3 ein. Für sie ist der Programmierteil
eher als Auffrischung bzw. Python Crash Course bei Vorkenntnissen in anderen Sprachen gedacht. Sie beenden den Kurs
dann mit Session 6, nach Einblicken ins API Design, Datenbanken und Software/Web Security mit einem prototypischen
Softwareentwicklungsprojekt. Für alle Teilnehmer:innen wird es neben den Präsenzterminen auch online Support-Meetings
und Treffen von Lerngruppen geben.

Dar wir im begrenzten Scope dieser Lehrveranstaltung weder einen vollständigen Einblick in alle Aspekte der
Programmiersprache Python, noch in alle Konzepte agiler Softwareentwicklung geben können, versuchen wir über die
eigentlichen LV-Inhalte hinaus auch weiterführende Ressourcen mitzugeben, für alle die sich im Thema noch weiter
vertiefen wollen.

Das primäre Ziel dieser Lehrveranstaltung ist, dass die Teilnehmer:innen danach selbstständig praxisrelevante Programme
im Rahmen des weiteren Masterstudium an der FH Wien schreiben können, und ein Gefühl für die Komplexität
moderner Softwareentwicklungsprozesse bekommen, um in weiterer Folge die besten Voraussetzungen und Rahmenbedingungen
für innovative Entwicklungsprozesse schaffen zu können.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   session1
   session2
   session3
   session4
   session5
   session6
   group_project_di
   group_project_dti
   resources

Diese Dokumentation wurde mit `Sphinx <https://www.sphinx-doc.org>`_, dem *Python Documentation Generator* erstellt.
Sphinx ist selbst in Python geschrieben, und kann einfach per ``pip install sphinx`` installiert werden. Wir verwenden
darüber hinaus noch den `MyST Parser <https://myst-parser.readthedocs.io>`_ um Files für die einzelnen Abschnitte auch
in `Markdown <https://commonmark.org/>`_ schreiben zu können.
`linkify-it-py <https://myst-parser.readthedocs.io/en/latest/syntax/optional.html#linkify>`_ wird verwendet um einfache
URLs auch automagisch in Links umzuwandeln. Als Theme haben wir auf das verbreitete ``sphinx_rtd_theme`` zurückgegriffen,
das auf `readthedocs.io <https://readthedocs.io>`_ von vielen Open Source Softwareprojekten zur Dokumentation verwendet wird. Als Erweiterung
haben wir aber auch noch ``sphinx_rtd_dark_mode`` hinzugefügt, damit Leser*innen in der Ansicht zwischen light und dark
mode wechseln können (siehe rechts unten den kreisrunden Button mit einem Mond/Sonne Symbol). Die Theme-bezogenen Links
finden sich im Footer.

Der Source Text dieser Dokumentation, inklusive der Sphinx config, um diesen Output zu generieren, befinden sich auf
https://gitlab.com/jackieklaura/fh-wien-software-engineering
