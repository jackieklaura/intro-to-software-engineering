# Literatur, Referenzen und Ressourcen

* Offizielle Python Dokumentation:
    * Zur Übersicht: [Python 3.11.1 documentation](https://docs.python.org/3/)
    * Zum Einstieg: [The Python Tutorial](https://docs.python.org/3/tutorial/index.html)
    * Zum Nachschlagen:
        * [The Python Language Reference](https://docs.python.org/3/reference/index.html)
        * [The Python Standard Library](https://docs.python.org/3/library/index.html)
    * Weitere Ressourcen für Einsteiger\*innen aus dem Python Wiki: [Beginner's Guide to Python](https://wiki.python.org/moin/BeginnersGuide)
* Gratis / frei verfügbare Inhalte:
    * [Al Sweigart: Automate the Boring Stuff with Python](https://automatetheboringstuff.com)
    * [FreeCodeCamp: Scientific Computing with Python](https://www.freecodecamp.org/learn/scientific-computing-with-python/)
    * [w3schools: Python Tutorial](https://www.w3schools.com/python/default.asp)
    * [PyLadies Vienna: Beginners Course](https://pyladies.at/2022/pyladies-en-vienna-2022-autumn/)
    * [Peter Knobloch: Programming With Python](http://knobot.net/programming_with_python/index.html)
* Mit Abo-Bezahlmodell und gratis trial period:
    * [Real Python Tutorials](https://realpython.com)
    * [Python Morsels](https://www.pythonmorsels.com) 


## Weiterführende Ressourcen:

* zu Software Engineering:
    * refactoring.guru: [Design Patterns](https://refactoring.guru/design-patterns)
* Secure Development Life Cycle, Secure Coding & IT Security in General:
    * [SheHacksPurple](https://shehackspurple.ca), with lots of talks and the awesome book _Alice and Bob Learn Application Security_
* für einen Einstieg in Data Science & Machine Learning:
    * [kaggle.com](https://www.kaggle.com/)
    * FreeCodeCamp: [Machine Learning with Python](https://www.freecodecamp.org/learn/machine-learning-with-python/)