from factorial import factorial

factorial_list = [1, 1]
for n in range(2, 501):
    factorial_list.append(n * factorial_list[n-1])


def test_factorial():
    for i in range(0, 500):
        assert factorial(i) == factorial_list[i]
