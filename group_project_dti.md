# Final group project [DTI]

Für das Abschlussprojekt in _Agile Software Engineering_ erstellt jede Lerngruppe
(idealerweise bestehend aus 4 Personen) eine prototypische Web-Applikation mit folgenden
**4 Komponenten**:

* **Datenbank** (z.B. SQLite)
* **Backend** (das auf die Datenbank zugreift und auf requests wartet, z.B. mit Flask)
* **API/Schnittstelle** für Webanwendungen (REST API)
* **Frontend** (entweder als einfache Web page, oder als Python CLI client)

Da die Komponenten und deren Design und Implementierung nicht immer ganz klar
voneinander getrennt werden können, würde es sich auch anbieten in 2 2er-Teams
zu arbeiten, von dem sich eines um die Datenbank und das Backend, und das andere
um die API-Definition und das Frontend kümmert.

Wesentlich ist, dass das Projekt in einem git repository entwickelt werden soll
(z.B. auf GitLab oder GitHub). Das Repo muss nicht öffentlich zugänglich sein,
die LV-Leiterinnen müssen aber Zugriff darauf haben. In der Praxis hat sich aber
die Verwendung eines öffentlichen Repos auf GitLab oder GitHub gut erprobt - sofern
nicht irgendwelche spezifischen Gründe dagegen sprechen.

Das Projekt muss **bis zur Abschlusseinheit umgesetzt** werden, und wird in der
Abschlusseinheit **von der Gruppe auch präsentiert**.

## Scope des Projekts

Es geht hier nicht um eine production-ready web app für einen tatsächlichen
use case, sondern um einen Prototyp, bzw. ein MVP einer concept app, mit der getestet
werden kann, ob die generelle Idee der App für die user base tatsächlich Sinn macht.
Die konkrete Aufgabe der App ist dabei ganz den Gruppen überlassen, allerdings ist
eine kurze Rücksprache mit den LV-Leiterinnen sinnvoll, um den Aufwand auch etwas
einzugrenzen und abzustecken.

Insbesondere beim Frontend käme in einer klassischen Web-Applikation freilich eine
entsprechende HTML/CSS/JS-basierte Seite zur Verwendung. Da wir uns mit diesen
Technologien im Rahmen der Lehrveranstaltung nicht auseinandergesetzt haben, ist
alternativ auch ein einfacher CLI client mit Python geschrieben in Ordnung. Oder
ihr erstellt eine einfache GUI mit Python, wenn ihr es optisch ansprechender
gestalten wollt.

Die Weboberfläche sollte von der Gruppe eher nur umgesetzt werden, wenn zumindest eine
Person wirklich Lust drauf hat sich auch mit dieser Materie auseinanderzusetzen,
oder vielleicht sogar schon etwas Erfahrung mit HTML/CSS und JavaScript hat.

Als Vorlage haben wir auch ein Beispielprojekt entwickelt, das den Scope auf jeden
Fall erfüllt und sogar zwei alternative Frontends hat - einmal eine simple Python CLI,
einmal ein einfaches Web-Interface mit drei HTML-Seiten. Dabei wird die Idee eines
Food Sharing Netzwerkes getestet. Also ganz im Sinne von Agile/MVP ein Prototyp bzw.
eine erste Concept App, um das angedachte Konzept in einem Praxistest zu überprüfen.
Das Projekt findet ihr in einem öffentlichen GitLab repo. Es ist der erste Link in
der folgenden Sammlung an hilfreichen Ressourcen.

Die Sammlung der Ressource fokussiert auf einem Backend mit Flask und SQLite, weil
dies vermutlich der einfachste Ansatz ist. Grundsätzlich steht es euch aber frei auch
andere Frameworks oder Technologien zu verwenden, als die von uns vorgeschlagenen,
wenn ihr euch diese entsprechend aneignet oder gar schon Vorerfahrungen damit habt.

## Hilfreiche Ressourcen

* [FH Wien Software Engineering Example Project](https://gitlab.com/jackieklaura/fh-wien-example-project) -
  ein von jackie erstelltes Repository auf GitLab, mit einer Web-App die alle Kriterien
  für das Projekt erfüllt. Ihr könnt diese als Vorlage nehmen und für euer Projekt
  abwandeln, oder nur als Anregung bzw. Referenz verwenden, wenn es darum geht, wie ihr
  mit Flask, SQLite, requests oder auch HTML/CSS/JS arbeiten könnt.
  * Video Recordings für das Example Project:
    * [How to run it](https://wolke.tantemalkah.at/s/pPr7mrsozZGoYKL) (~45min/165MB)
    * [How to git flow](https://wolke.tantemalkah.at/s/GpHpzBcCxYAsRfy) (~22min/95MB)
* https://sqlitebrowser.org - ein Browser/Client für SQLite Datenbanken
* [Creating an API REST with Python, Flask and SQLite3](https://parzibyte.me/blog/en/2020/11/12/creating-api-rest-with-python-flask-sqlite3/) -
  ein Tutorial von Luis Cabrera Benito, das mit minimalen Mitteln demonstriert wie eine
  RESTful API mit Flask und SQLite 3 gebaut werden kann - ohne weitere Libraries zu verwenden.
  Auch das oben verlinkte Beispielprojekt hat sich hier viele Anregungen geholt.
* [How to Create a RESTful API with Flask in Python](https://thepythoncode.com/article/create-a-restful-api-with-flask-in-python) -
  auch ein Tutorial zum Erstellen einer RESTful API mit Flask, von Abdeladim Fadheli auf The Python Code,
  in dem Fall aber unter Verwendung weitere Libraries wie [Flask-RESTful](https://flask-restful.readthedocs.io)
  und [SQLAlchemy](https://www.sqlalchemy.org/). Daher vielleicht für den Anfang etwas viel,
  dafür gibt es aber weitere Einblicke und ist näher an dem dran, wie in der Praxis produktiv
  eingesetzte RESTful APIs mit Flask erstellt werden.
* [How to Work with SQLite in Python – A Handbook for Beginners](https://www.freecodecamp.org/news/work-with-sqlite-in-python-handbook/) -
  vollständiges Tutorial/Handbuch von Ashutosh Krishna auf freeCodeCamp, das demonstriert wie mit `sqlite3` in
  Python gearbeitet werden kann.
* [How to Write Better Git Commit Messages – A Step-By-Step Guide](https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/) -
  ein Ratgeber bzw. Intro von Natalie Pina auf freeCodeCamp, ins Schreiben von guten commit messages - gibt auch
  Einblick in die [Conventional Commits](https://www.conventionalcommits.org) Spezifikation,
  an der sich auch die commit messages im oben verlinkten Beispielprojekt orientieren.
* [A successful Git branching model](https://nvie.com/posts/a-successful-git-branching-model/) -
  Artikel von Vincent Driessen aus dem Jahr 2010, der das nach wie vor populäre _git flow_
  branching model begründet, an dem sich auch das Beispielprojekt oben orientiert.
